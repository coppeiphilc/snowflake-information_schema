connection: "snowflake_information_schema"

# include all the views
include: "/views/**/*.view"

datagroup: snowflake_obd_information_schema_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: snowflake_obd_information_schema_default_datagroup

explore: applicable_roles {}

explore: columns {}

explore: databases {}

explore: enabled_roles {}

explore: external_tables {}

explore: file_formats {}

explore: functions {}

explore: information_schema_catalog_name {}

explore: load_history {}

explore: object_privileges {}

explore: pipes {}

explore: procedures {}

explore: referential_constraints {}

explore: replication_databases {}

explore: schemata {}

explore: sequences {}

explore: stages {}

explore: table_constraints {}

explore: table_privileges {}

explore: table_storage_metrics {}

explore: tables {}

explore: usage_privileges {}

explore: views {}
